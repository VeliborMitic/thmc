import asyncio
import pytest

from thcouch.core.server import get_server, get_all_dbs
from thcouch.core.db import put_db, delete_db
from thcouch.orm import CouchClient
from thcouch.orm.decl import CouchLoader

from thmc.model_controller import ModelController

COUCH_URI = 'http://tangledhub:tangledhub@couchdb-thmc-test:5984'


@pytest.fixture(scope='session')
def event_loop(request):
    """Create an instance of the default event loop for each test case."""
    loop = asyncio.get_event_loop_policy().new_event_loop()
    yield loop
    loop.close()


@pytest.fixture(scope='session', autouse=True)
@pytest.mark.asyncio
async def couch_session_teardown(request):
    all_dbs = (await get_all_dbs(uri=COUCH_URI)).unwrap()

    for db in all_dbs.dbs:
        (await delete_db(uri=COUCH_URI, db=db)).unwrap()

    yield


@pytest.fixture
@pytest.mark.asyncio
async def model_controller(request):
    name = request.function.__name__
    COUCH_DATABASE = name
    # COUCH_DATABASE_URL = f'{COUCH_URI}/{COUCH_DATABASE}'

    # check if couch server is running
    while True:
        get_server_ = (await get_server(COUCH_URI)).unwrap()
        break

    put_db_ = (await put_db(uri=COUCH_URI, db=COUCH_DATABASE)).unwrap()
    client = CouchClient(COUCH_URI)
    LOADER = CouchLoader(client.database(COUCH_DATABASE).unwrap(), path='profile.yaml')

    AdminProfile = LOADER.AdminProfile

    class AdminProfileMC(ModelController):
        model_type = AdminProfile

    admin_mc = AdminProfileMC(AdminProfile)

    yield admin_mc