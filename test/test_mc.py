import pytest

from thcouch.orm import CouchAttachment
from thcouch.orm.decl import BaseModel
from thresult import ResultException


@pytest.fixture(scope='module')
def test_doc_list():
    """
    This function yields list of dicts for testing purposes
    """

    yield [{'first_name': 'Alice', 'last_name': 'AliceFirst', 'email': 'alice_first@testmc.com'},
           {'first_name': 'Alice', 'last_name': 'AliceSecond', 'email': 'alice_second@testmc.com'},
           {'first_name': 'Bob', 'last_name': 'BobFirst', 'email': 'bob_first@testmc.com'},
           {'first_name': 'Clark', 'last_name': 'ClarkFirst', 'email': 'clark_first@testmc.com'},
           {'first_name': 'Dane', 'last_name': 'DaneFirst', 'email': 'dane_first@testmc.com'},
           {'first_name': 'Elene', 'last_name': 'EleneFirst', 'email': 'elene_first@testmc.com'},
           {'first_name': 'Elene', 'last_name': 'EleneSecond', 'email': 'elene_second@testmc.com'},
           {'first_name': 'Elene', 'last_name': 'EleneThird', 'email': 'elene_third@testmc.com'}]


@pytest.mark.asyncio
async def test_create(model_controller):
    """
    This function tests creation of a new model with model_controller.create()
    """
    admin_instance = await model_controller.create(model_data={'email': 'admin@testmc.com'})
    assert type(admin_instance) == model_controller.model_type
    assert admin_instance.email == 'admin@testmc.com'


@pytest.mark.asyncio
async def test_get(model_controller):
    admin_instance = await model_controller.create(model_data={'email': 'admin@testmc.com'})

    get_admin_: model_controller.model_type = await model_controller.get(model_id=admin_instance._id)
    assert type(get_admin_) == model_controller.model_type
    assert get_admin_.email == 'admin@testmc.com'


@pytest.mark.asyncio
@pytest.mark.xfail(raises=ResultException, strict=True)
async def test_get_by_non_existing_id(model_controller):
    get_admin_: model_controller.model_type = await model_controller.get(model_id='0')


@pytest.mark.asyncio
async def test_get_all(model_controller):
    admin_instance_1 = await model_controller.create(model_data={'email': 'admin1@testmc.com'})
    admin_instance_2 = await model_controller.create(model_data={'email': 'admin2@testmc.com'})

    model_type = model_controller.model_type

    admins: list[model_type] = await model_controller.get_all()
    assert all(isinstance(elem, model_type) for elem in admins)


@pytest.mark.asyncio
async def test_bulk_docs(model_controller, test_doc_list):
    model_type = model_controller.model_type

    admin_9: model_type = model_type(
        first_name='Franklin',
        last_name='FranklinFirst',
        email='franklin_first')
    admin_10: model_type = model_type(
        first_name='George',
        last_name='GeorgeFirst',
        email='george_first')

    doc_dict_list: list[dict] = await model_controller.bulk_docs(docs=[*test_doc_list, admin_9, admin_10])
    assert isinstance(doc_dict_list, list)
    assert all(isinstance(elem, dict) for elem in doc_dict_list)
    assert all(k in elem for k in ('ok', 'id', 'rev') for elem in doc_dict_list)


@pytest.mark.asyncio
async def test_bulk_get(model_controller, test_doc_list):
    doc_dict_list: list[dict] = await model_controller.bulk_docs(docs=test_doc_list)

    assert isinstance(doc_dict_list, list)
    assert len(doc_dict_list) == len(test_doc_list)
    assert all(isinstance(elem, dict) for elem in doc_dict_list)
    assert all(k in elem for k in ('ok', 'id', 'rev') for elem in doc_dict_list)

    # bulk get
    docs: list[dict[str, str]] = [{'id': f'{e["id"]}'} for e in doc_dict_list]

    res: list[BaseModel] = await model_controller.bulk_get(docs=docs)
    assert isinstance(res, list)
    assert len(res) == len(docs)
    assert all(isinstance(elem, BaseModel) for elem in res)


@pytest.mark.asyncio
async def test_search(model_controller, test_doc_list):
    doc_dict_list: list[dict] = await model_controller.bulk_docs(docs=test_doc_list)

    assert isinstance(doc_dict_list, list)
    assert len(doc_dict_list) == len(test_doc_list)
    assert all(isinstance(elem, dict) for elem in doc_dict_list)
    assert all(k in elem for k in ('ok', 'id', 'rev') for elem in doc_dict_list)

    admins, bookmark, warning = await model_controller.search(selector={'first_name': "Alice"})
    assert isinstance(admins, list)
    assert isinstance(bookmark, str)
    assert isinstance(warning, str)
    assert all(isinstance(elem, BaseModel) for elem in admins)
    assert all(elem['first_name'] == 'Alice' for elem in admins)


@pytest.mark.asyncio
async def test_search_non_existing(model_controller):
    admin_instance_1 = await model_controller.create(model_data={'email': 'admin1@testmc.com'})
    admin_instance_2 = await model_controller.create(model_data={'email': 'admin2@testmc.com'})

    admins, bookmark, warning = await model_controller.search(selector={'email': 'admin@non-exsisting.com'})
    assert isinstance(admins, list)
    assert isinstance(bookmark, str)
    assert isinstance(warning, str)
    assert not admins


@pytest.mark.asyncio
async def test_delete_model_id_passed(model_controller):
    admin_instance = await model_controller.create(model_data={'email': 'admin@testmc.com'})

    assert type(admin_instance) == model_controller.model_type
    assert admin_instance.email == 'admin@testmc.com'

    admin_deleted = await model_controller.delete(model_id=admin_instance._id)
    assert isinstance(admin_deleted, model_controller.model_type)


@pytest.mark.asyncio
async def test_delete_model_object_passed(model_controller):
    admin_instance = await model_controller.create(model_data={'email': 'admin@testmc.com'})
    assert isinstance(admin_instance, model_controller.model_type)
    assert admin_instance.email == 'admin@testmc.com'

    admin_deleted = await model_controller.delete(model=admin_instance)
    assert isinstance(admin_deleted, model_controller.model_type)


@pytest.mark.asyncio
@pytest.mark.xfail(raises=ResultException, strict=True)
async def test_delete_non_existing_model(model_controller):
    admin_instance = await model_controller.create(model_data={'email': 'admin@testmc.com'})
    assert isinstance(admin_instance, model_controller.model_type)
    assert admin_instance.email == 'admin@testmc.com'

    admin_deleted = await model_controller.delete(model_id='non-existing-id')


@pytest.mark.asyncio
async def test_update_model_id_passed(model_controller):
    admin_instance = await model_controller.create(model_data={'email': 'admin@testmc.com'})
    assert isinstance(admin_instance, model_controller.model_type)
    assert admin_instance.email == 'admin@testmc.com'

    updated_admin_instance = await model_controller.update(model_id=admin_instance._id,
                                                           update_data={'email': 'updated@testmc.com'})

    assert isinstance(updated_admin_instance, model_controller.model_type)
    assert updated_admin_instance.email == 'updated@testmc.com'
    assert updated_admin_instance._rev != admin_instance._rev


@pytest.mark.asyncio
async def test_update_model_object_passed(model_controller):
    admin_instance = await model_controller.create(model_data={'email': 'admin@testmc.com'})
    assert isinstance(admin_instance, model_controller.model_type)
    assert admin_instance.email == 'admin@testmc.com'

    updated_admin_instance = await model_controller.update(model=admin_instance,
                                                           update_data={'email': 'updated@testmc.com'})

    assert isinstance(updated_admin_instance, model_controller.model_type)
    assert updated_admin_instance.email == 'updated@testmc.com'
    assert updated_admin_instance._rev != admin_instance._rev


@pytest.mark.asyncio
@pytest.mark.xfail(raises=ResultException, strict=True)
async def test_update_non_existing_id(model_controller):
    admin_instance = await model_controller.create(model_data={'email': 'admin@testmc.com'})
    assert isinstance(admin_instance, model_controller.model_type)
    assert admin_instance.email == 'admin@testmc.com'

    updated_admin_instance = await model_controller.update(model_id='non-existing-id',
                                                           update_data={'email': 'updated@testmc.com'})


@pytest.mark.asyncio
@pytest.mark.xfail(raises=ResultException, strict=True)
async def test_update_non_existing_field(model_controller):
    admin_instance = await model_controller.create(model_data={'email': 'admin@testmc.com'})
    assert isinstance(admin_instance, model_controller.model_type)
    assert admin_instance.email == 'admin@testmc.com'

    updated_admin_instance = await model_controller.update(model_id='admin_instance._id',
                                                           update_data={'address': 'some-address'})


@pytest.mark.asyncio
async def test_add_attachment_model_id(model_controller):
    admin_instance = await model_controller.create(model_data={'email': 'admin@testmc.com'})
    assert isinstance(admin_instance, model_controller.model_type)
    assert admin_instance.email == 'admin@testmc.com'

    test_file_name = 'test_file.txt'
    with open(test_file_name, 'w') as f:
        f.write('test file content')

    with open(test_file_name, 'rb') as reader:
        content = reader.read()

    admin, attachment = await model_controller.add_attachment(model_id=admin_instance._id,
                                                              attachment_name=test_file_name,
                                                              body=content)
    assert isinstance(admin, model_controller.model_type)
    assert isinstance(attachment, CouchAttachment)


@pytest.mark.asyncio
async def test_add_attachment_model_object(model_controller):
    admin_instance = await model_controller.create(model_data={'email': 'admin@testmc.com'})
    assert isinstance(admin_instance, model_controller.model_type)
    assert admin_instance.email == 'admin@testmc.com'

    test_file_name = 'test_file.txt'
    with open(test_file_name, 'w') as f:
        f.write('test file content')

    with open(test_file_name, 'rb') as reader:
        content = reader.read()

    admin, attachment = await model_controller.add_attachment(model=admin_instance,
                                                              attachment_name=test_file_name,
                                                              body=content)
    assert isinstance(admin, model_controller.model_type)
    assert isinstance(attachment, CouchAttachment)


@pytest.mark.asyncio
async def test_get_attachment_model_id_passed(model_controller):
    admin_instance = await model_controller.create(model_data={'email': 'admin@testmc.com'})
    assert isinstance(admin_instance, model_controller.model_type)
    assert admin_instance.email == 'admin@testmc.com'

    test_file_name = 'test_file.txt'
    with open(test_file_name, 'w') as f:
        f.write('test file content')

    with open(test_file_name, 'rb') as reader:
        content = reader.read()

    admin, attachment = await model_controller.add_attachment(model=admin_instance,
                                                              attachment_name=test_file_name,
                                                              body=content)
    assert isinstance(admin, model_controller.model_type)
    assert isinstance(attachment, CouchAttachment)

    att: CouchAttachment = await model_controller.get_attachment(model_id=admin._id,
                                                                 attachment_name=test_file_name)

    assert isinstance(att, CouchAttachment)
    assert att['attachment_name'] == test_file_name
    assert att['body'] == b'test file content'


@pytest.mark.asyncio
async def test_get_attachment_model_object_passed(model_controller):
    admin_instance = await model_controller.create(model_data={'email': 'admin@testmc.com'})
    assert isinstance(admin_instance, model_controller.model_type)
    assert admin_instance.email == 'admin@testmc.com'

    test_file_name = 'test_file.txt'
    with open(test_file_name, 'w') as f:
        f.write('test file content')

    with open(test_file_name, 'rb') as reader:
        content = reader.read()

    admin, attachment = await model_controller.add_attachment(model=admin_instance,
                                                              attachment_name=test_file_name,
                                                              body=content)
    assert isinstance(admin, model_controller.model_type)
    assert isinstance(attachment, CouchAttachment)

    att: CouchAttachment = await model_controller.get_attachment(model=admin,
                                                                 attachment_name=test_file_name)

    assert isinstance(att, CouchAttachment)
    assert att['attachment_name'] == test_file_name
    assert att['body'] == b'test file content'


@pytest.mark.asyncio
async def test_update_attachment_model_id_passed(model_controller):
    admin_instance = await model_controller.create(model_data={'email': 'admin@testmc.com'})
    assert isinstance(admin_instance, model_controller.model_type)
    assert admin_instance.email == 'admin@testmc.com'

    test_file_name = 'test_file.txt'
    with open(test_file_name, 'w') as f:
        f.write('test file content')

    with open(test_file_name, 'rb') as reader:
        content: bytes = reader.read()

    admin, attachment = await model_controller.add_attachment(model=admin_instance,
                                                              attachment_name=test_file_name,
                                                              body=content)
    assert isinstance(admin, model_controller.model_type)
    assert isinstance(attachment, CouchAttachment)

    att: CouchAttachment = await model_controller.get_attachment(model=admin,
                                                                 attachment_name=test_file_name)

    assert isinstance(att, CouchAttachment)
    assert att['attachment_name'] == test_file_name
    assert att['body'] == b'test file content'

    # Update attachment
    test_file_name = 'test_file.txt'
    with open(test_file_name, 'w') as f:
        f.write('UPDATED test file content')

    with open(test_file_name, 'rb') as reader:
        content: bytes = reader.read()

    model, updated_attachment = await model_controller.update_attachment(model_id=admin._id,
                                                                         attachment_name=test_file_name,
                                                                         body=content)
    assert isinstance(model, model_controller.model_type)
    assert isinstance(updated_attachment, CouchAttachment)

    updated_att: CouchAttachment = await model_controller.get_attachment(model=admin,
                                                                         attachment_name=test_file_name)
    assert isinstance(model, model_controller.model_type)
    assert isinstance(updated_att, CouchAttachment)
    assert updated_att['body'] == b'UPDATED test file content'


@pytest.mark.asyncio
async def test_update_attachment_model_object_passed(model_controller):
    admin_instance = await model_controller.create(model_data={'email': 'admin@testmc.com'})
    assert isinstance(admin_instance, model_controller.model_type)
    assert admin_instance.email == 'admin@testmc.com'

    test_file_name = 'test_file.txt'
    with open(test_file_name, 'w') as f:
        f.write('test file content')

    with open(test_file_name, 'rb') as reader:
        content: bytes = reader.read()

    admin, attachment = await model_controller.add_attachment(model=admin_instance,
                                                              attachment_name=test_file_name,
                                                              body=content)
    assert isinstance(admin, model_controller.model_type)
    assert isinstance(attachment, CouchAttachment)

    att: CouchAttachment = await model_controller.get_attachment(model=admin,
                                                                 attachment_name=test_file_name)

    assert isinstance(att, CouchAttachment)
    assert att['attachment_name'] == test_file_name
    assert att['body'] == b'test file content'

    # Update attachment
    test_file_name = 'test_file.txt'
    with open(test_file_name, 'w') as f:
        f.write('UPDATED test file content')

    with open(test_file_name, 'rb') as reader:
        content: bytes = reader.read()

    model, updated_attachment = await model_controller.update_attachment(model=admin,
                                                                         attachment_name=test_file_name,
                                                                         body=content)
    assert isinstance(model, model_controller.model_type)
    assert isinstance(updated_attachment, CouchAttachment)

    updated_att: CouchAttachment = await model_controller.get_attachment(model=admin,
                                                                         attachment_name=test_file_name)
    assert isinstance(model, model_controller.model_type)
    assert isinstance(updated_att, CouchAttachment)
    assert updated_att['body'] == b'UPDATED test file content'


@pytest.mark.asyncio
async def test_remove_attachment_model_id_passed(model_controller):
    admin_instance = await model_controller.create(model_data={'email': 'admin@testmc.com'})
    assert isinstance(admin_instance, model_controller.model_type)
    assert admin_instance.email == 'admin@testmc.com'

    test_file_name = 'test_file.txt'
    with open(test_file_name, 'w') as f:
        f.write('test file content')

    with open(test_file_name, 'rb') as reader:
        content = reader.read()

    admin, attachment = await model_controller.add_attachment(model=admin_instance,
                                                              attachment_name=test_file_name,
                                                              body=content)
    assert isinstance(admin, model_controller.model_type)
    assert isinstance(attachment, CouchAttachment)
    assert admin['_attachments']

    remove_att_res: BaseModel = await model_controller.remove_attachment(model_id=admin._id,
                                                                         attachment_name=test_file_name)

    assert isinstance(remove_att_res, BaseModel)
    assert not remove_att_res['_attachments']


@pytest.mark.asyncio
async def test_remove_attachment_model_object_passed(model_controller):
    admin_instance = await model_controller.create(model_data={'email': 'admin@testmc.com'})
    assert isinstance(admin_instance, model_controller.model_type)
    assert admin_instance.email == 'admin@testmc.com'

    test_file_name = 'test_file.txt'
    with open(test_file_name, 'w') as f:
        f.write('test file content')

    with open(test_file_name, 'rb') as reader:
        content = reader.read()

    admin, attachment = await model_controller.add_attachment(model=admin_instance,
                                                              attachment_name=test_file_name,
                                                              body=content)
    assert isinstance(admin, model_controller.model_type)
    assert isinstance(attachment, CouchAttachment)
    assert admin['_attachments']

    remove_att_res: BaseModel = await model_controller.remove_attachment(model=admin,
                                                                         attachment_name=test_file_name)

    assert isinstance(remove_att_res, BaseModel)
    assert not remove_att_res['_attachments']
