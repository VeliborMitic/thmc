from abc import ABC
from thcouch.orm import CouchAttachment
from thcouch.orm.decl import BaseModel


class ModelController(ABC):
    model_type: BaseModel

    def __init__(self, model_type):
        self.model_type = model_type

    @classmethod
    async def create(cls, model_data: dict) -> BaseModel:
        model: cls.model_type = cls.model_type(**model_data)

        return (await cls.model_type.add(model)).unwrap()

    @classmethod
    async def get(cls, model_id: str, rev: None | str = None) -> BaseModel:
        model: cls.model_type = (await cls.model_type.get(docid=model_id, rev=rev)).unwrap()

        return model

    @classmethod
    async def get_all(cls) -> list[BaseModel]:
        models: list[cls.model_type] = (await cls.model_type.all()).unwrap()

        return models

    @classmethod
    async def bulk_docs(cls, docs: list[BaseModel | dict], new_edits: None | bool = None) -> list[dict]:
        models: list[dict] = (await cls.model_type.bulk_docs(docs=docs, new_edits=new_edits)).unwrap()

        return models

    @classmethod
    async def bulk_get(cls, docs: list[dict], revs: None | bool = None) -> list[BaseModel]:
        models: list[cls.model_type] = (await cls.model_type.bulk_get(docs=docs, revs=revs)).unwrap()

        return models

    @classmethod
    async def search(cls,
                     selector: dict,
                     limit: int = None,
                     skip: int = None,
                     sort: str = None,
                     fields: list = None,
                     use_index: str = None,
                     bookmark: str = None,
                     update: bool = False) -> tuple[list[BaseModel], str, str]:
        """
        Returns list of all documents by given selector(query)
        """

        models, bookmark, warning = (await cls.model_type.find(selector=selector,
                                                               limit=limit,
                                                               skip=skip,
                                                               sort=sort,
                                                               fields=fields,
                                                               use_index=use_index,
                                                               bookmark=bookmark,
                                                               update=update)).unwrap()

        return models, bookmark, warning

    @classmethod
    async def delete(cls,
                     model_id: str = None,
                     model: BaseModel = None,
                     batch: None | str = None) -> BaseModel:
        model: cls.model_type = await cls.get(model_id=model_id) if not model and model_id else model

        return (await cls.model_type.delete(model, batch=batch)).unwrap()

    @classmethod
    async def update(cls,
                     update_data: dict,
                     model_id: str = None,
                     model: BaseModel = None,
                     protected_fields: list = None,
                     batch: None | str = None,
                     new_edits: None | bool = None) -> BaseModel:
        model: cls.model_type = await cls.get(model_id=model_id) if not model and model_id else model
        if protected_fields is None:
            protected_fields = []
        update_doc = {k: v for k, v in update_data.items() if v is not None and k not in protected_fields}

        return (await cls.model_type.update(model, doc=update_doc, batch=batch, new_edits=new_edits)).unwrap()

    @classmethod
    async def add_attachment(cls,
                             attachment_name: str,
                             body: bytes,
                             model_id: str = None,
                             model: BaseModel = None,
                             ):
        model: cls.model_type = await cls.get(model_id=model_id) if not model and model_id else model
        data_tuple: tuple[BaseModel, CouchAttachment] = \
            (await cls.model_type.add_attachment(model,
                                                 attachment_name=attachment_name,
                                                 body=body)).unwrap()
        return data_tuple

    @classmethod
    async def get_attachment(cls,
                             attachment_name: str,
                             model_id: str = None,
                             model: BaseModel = None,
                             range: str | None = None) -> CouchAttachment:
        model: cls.model_type = await cls.get(model_id=model_id) if not model and model_id else model
        attachment: CouchAttachment = (await cls.model_type.get_attachment(model,
                                                                           attachment_name=attachment_name,
                                                                           range=range)).unwrap()

        return attachment

    @classmethod
    async def update_attachment(cls,
                                attachment_name: str,
                                body: bytes,
                                model_id: str = None,
                                model: BaseModel = None,
                                ):
        model: cls.model_type = await cls.get(model_id=model_id) if not model and model_id else model
        update_tuple: tuple[BaseModel, CouchAttachment] = \
            (await cls.model_type.update_attachment(model,
                                                    attachment_name=attachment_name,
                                                    body=body)).unwrap()
        return update_tuple

    @classmethod
    async def remove_attachment(cls,
                                attachment_name: str,
                                model_id: str = None,
                                model: BaseModel = None,
                                batch: None | str = None) -> BaseModel:
        model: cls.model_type = await cls.get(model_id=model_id) if not model and model_id else model
        res_model: BaseModel = (await cls.model_type.remove_attachment(model,
                                                                       attachment_name=attachment_name,
                                                                       batch=batch)).unwrap()
        return res_model
